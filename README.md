# @loco-tts/espeak-showcase

**A speech synthesizer plugin for [loco-tts](https://gitlab.com/loco-tts/core).**

## On the shoulders of...

**This is an adaptation and partial rewrite of [meSpeak](https://www.masswerk.at/mespeak/) 2.0.7**, taken from [this zip](https://www.masswerk.at/mespeak/mespeak.zip?v=2.0.7).  
meSpeak is a work of Norbert Landsteiner. 
**meSpeak is itself based on [speak.js](https://github.com/kripken/speak.js)**, a port of [espeak](https://espeak.sourceforge.net) to the web.

**The engine.js, voice and config .json files are also from meSpeak.**

## Installation

`npm i @loco-tts/espeak`