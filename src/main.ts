import { Worker as W } from "@loco-tts/espeak";
const engine = new W("./engine.js");
engine.loadConfig("./default.json");
engine.loadVoice("./en.json");
engine.useTransferables();
let audioCtx;
document.getElementById("demo")?.addEventListener("click", async () => {
    console.time("TTS");
    if (!audioCtx) { audioCtx = new AudioContext(); };
    const { audiodata } = await engine.generate("Hello, world!", {});
    const audioBuffer = await audioCtx.decodeAudioData(audiodata);
    const source = audioCtx.createBufferSource();
    source.buffer = audioBuffer;
    console.timeEnd("TTS");
    source.connect(audioCtx.destination);
    source.start();
    console.log(source);
});