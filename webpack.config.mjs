import { resolve, dirname } from "path";
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));
export default (env, argv) => ({
  entry: {
    "main": "./src/main.ts",
    "engine": "./src/engine.js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /^@/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      }
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".ts", ".tsx"],
    alias: argv.mode === "development"
    ?  {
      // resolve library locally
      "@loco-tts/espeak": resolve(__dirname, "../espeak/dist/esm/index.js")
      // rewire mutual dependencies of library and showcase
    }
    : {
      // in production
      // do nothing - library needs to be installed
    }
  },
  output: {
    path: resolve(__dirname, `./public`),
    filename: "[name].js",
  },
  devServer: {
    static: resolve(__dirname, `./public`),
  },
});